//
//  MainViewTableViewCell.swift
//  Quantrics Test
//
//  Created by Robert John Alkuino on 8/13/21.
//

import UIKit

class MainViewTableViewCell: UITableViewCell {
    @IBOutlet weak var agencyLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func populateData(stops:Stops) {
        agencyLabel.text = stops.agency
        nameLabel.text = stops.name
    }
    
}
