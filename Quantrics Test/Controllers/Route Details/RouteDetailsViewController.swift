//
//  RouteDetailsViewController.swift
//  Quantrics Test
//
//  Created by Robert John Alkuino on 8/13/21.
//

import UIKit

class RouteDetailsViewController: UIViewController {
    
    var viewModel:RouteDetailsViewModel?
    
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let viewModel = viewModel {
            var concatStr = ""

            for data in viewModel.routeArray {
                
                concatStr += "Route: \(data.name ?? "") - \(data.stopTimes?.count ?? 0) stops \n\n"
                
                for x in data.stopTimes! {
                    concatStr += "🚦\(x.shape ?? "") - \(x.departure_time ?? "")\n"
                }
                
                concatStr += "\n\n"
            }
            
            textView.text = concatStr
        }
        
        
    }
    
    

}
