//
//  MainViewController.swift
//  Quantrics Test
//
//  Created by Robert John Alkuino on 8/4/21.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var stationsTable: UITableView!
    
    let viewModel = MainViewModel()
    
    let searchController = UISearchController(searchResultsController: nil)
    
    private var isSearchBarEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    private var isFiltering: Bool {
        return searchController.isActive && !isSearchBarEmpty
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Stops"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        stationsTable.rowHeight = UITableView.automaticDimension
        stationsTable.register(cell: MainViewTableViewCell.self)
        
        let loader = loader(controller: self)
        
        viewModel.fetchData(completion: { [weak self] in
            DispatchQueue.main.async {
                stopLoader(loader: loader)
                self?.stationsTable.reloadData()
            }
        })
    }
}

extension MainViewController:UITableViewDataSource,UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isFiltering {
            return viewModel.filteredRoutes.stops.count
        }
            
        return viewModel.routesData?.stops.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MainViewTableViewCell.reuseIdentifier,
                                                 for: indexPath) as! MainViewTableViewCell
        
        if isFiltering {
            cell.populateData(stops: viewModel.filteredRoutes.stops[indexPath.row])
        } else {
            if let busStops = viewModel.routesData?.stops[indexPath.row] {
                cell.populateData(stops: busStops)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var _busStops = viewModel.routesData?.stops[indexPath.row]
        
        if isFiltering {
            _busStops = viewModel.filteredRoutes.stops[indexPath.row]
        }
        
        if let busStops = _busStops {
            if let route = busStops.routes {
                let vc = RouteDetailsViewController()
                let vm = RouteDetailsViewModel()
                vm.routeArray = route
                vc.viewModel = vm
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }
}

extension MainViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
    
    func filterContentForSearchText(_ searchText: String) {
        
        viewModel.filteredRoutes.name = "Sample"
        viewModel.filteredRoutes.stops = viewModel.routesData!.stops.filter{ (stops: Stops) -> Bool in
            return stops.name!.lowercased().contains(searchText.lowercased())
        }
        
        
        
        
        stationsTable.reloadData()
    }
      
    
}
