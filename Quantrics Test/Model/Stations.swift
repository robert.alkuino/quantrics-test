//
//  Routes.swift
//  Quantrics Test
//
//  Created by Robert John Alkuino on 8/5/21.
//

import UIKit
import ObjectMapper

class Stations: Mappable {
    var name: String = ""
    var stops: [Stops] = []
    
    required init?(map: Map) {}
    
    init(name: String, stops: [Stops] = []){
        self.name = name
        self.stops = stops
    }

    func mapping(map: Map) {
        name    <- map["name"]
        stops   <- map["stops"]
    }
}
