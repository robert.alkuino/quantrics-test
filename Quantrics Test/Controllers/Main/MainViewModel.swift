//
//  MainViewModel.swift
//  Quantrics Test
//
//  Created by Robert John Alkuino on 8/13/21.
//

import UIKit

class MainViewModel: NSObject {

    var routesData:Stations?
    var filteredRoutes:Stations = Stations(name: "")
    
    func fetchData(completion:@escaping () -> Void) {
        APIService<Stations>.fetch(completion: { [weak self] val in
            self?.routesData = val
            completion()
        })
    }
}
