//
//  APIService.swift
//  Quantrics Test
//
//  Created by Robert John Alkuino on 8/5/21.
//

import UIKit
import ObjectMapper

class APIService<T: Mappable> {
    
    static func fetch(completion:@escaping (T) -> Void) {
        
        if let url = URL(string: "https://myttc.ca/finch_station.json") {
            var request = URLRequest(url:url)
            
            request.httpMethod = "GET"
            
            URLSession.shared.dataTask(with: request, completionHandler: { (data: Data?,
                response:URLResponse?, error: Error?) -> Void in
                do {
                    guard data != nil else {
                        return
                    }
                    if let results: NSDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments  ) as? NSDictionary {
                        
                        if let object = Mapper<T>().map(JSONObject: results) {
                            completion(object)
                            return
                        }
                    }


                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }).resume()
        }
    }
}


