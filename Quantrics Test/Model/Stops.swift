//
//  Route.swift
//  Quantrics Test
//
//  Created by Robert John Alkuino on 8/5/21.
//

import UIKit
import ObjectMapper

struct Stops: Mappable {
    var agency: String?
    var name: String?
    var routes:[Routes]?
    var uri:String?
    
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        agency    <- map["agency"]
        name      <- map["name"]
        routes    <- map["routes"]
        uri       <- map["uri"]
        
    }
}
