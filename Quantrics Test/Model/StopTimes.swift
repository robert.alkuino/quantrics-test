//
//  StopTimes.swift
//  Quantrics Test
//
//  Created by Robert John Alkuino on 8/5/21.
//

import UIKit
import ObjectMapper

struct StopTimes: Mappable {
    var departure_time: String?
    var departure_timestamp: String?
    var service_id:String?
    var shape:String?
    
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        departure_time           <- map["departure_time"]
        departure_timestamp      <- map["departure_timestamp"]
        service_id               <- map["service_id"]
        shape                    <- map["shape"]
    }
}
