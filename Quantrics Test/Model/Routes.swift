//
//  Routes.swift
//  Quantrics Test
//
//  Created by Robert John Alkuino on 8/5/21.
//

import UIKit
import ObjectMapper

struct Routes: Mappable {
    var name: String?
    var routeGroupId: String?
    var stopTimes:[StopTimes]?
    
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        name            <- map["name"]
        routeGroupId    <- map["route_group_id"]
        stopTimes       <- map["stop_times"]
        
    }
}
